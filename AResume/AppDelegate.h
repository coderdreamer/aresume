//
//  AppDelegate.h
//  AResume
//
//  Created by William Remaerd on 12/19/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASINetworkQueue.h"
@class CDSideviewController, Candidate, ASINetworkQueue;
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Candidate *candidate;
@property (strong, nonatomic) ASINetworkQueue* requestQueue;
@property (strong, nonatomic) CDSideviewController* sideiewController;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
+ (Candidate*)sharedCnadidate;
@end
