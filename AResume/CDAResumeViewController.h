//
//  CDAResumeViewController.h
//  AResume
//
//  Created by William Remaerd on 12/24/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSideviewController.h"
#define kIdentifierCandidate @"Candidate"
#define kIdentifierExperience @"Experience"
#define kIdentifierSkill @"Skill"
#define kIdentifierHonour @"Honour"

@interface CDAResumeViewController : CDSideviewController
- (id)initWithAResumeStyle;
@end
