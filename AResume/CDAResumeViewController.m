//
//  CDAResumeViewController.m
//  AResume
//
//  Created by William Remaerd on 12/24/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDAResumeViewController.h"
#import "CDCandidateController.h"
#import "CDExperienceController.h"
#import "CDSkillController.h"
#import "CDHonourController.h"

@implementation CDAResumeViewController
- (id)initWithAResumeStyle
{
    CDCandidateController *controller = [[CDCandidateController alloc] initWithSideviewController:self embededNavigation:YES];
    self = [super initWithRootViewController:controller identifier:kIdentifierCandidate];
    [controller release];
    if (self) {
        [self registerControllerWithIdentifier:kIdentifierExperience conClass:[CDExperienceController class] embededInNavigation:YES];
        [self registerControllerWithIdentifier:kIdentifierSkill conClass:[CDSkillController class] embededInNavigation:YES];
        [self registerControllerWithIdentifier:kIdentifierHonour conClass:[CDHonourController class] embededInNavigation:YES];
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
}

@end
