//
//  CDURL.h
//  AResume
//
//  Created by William Remaerd on 12/29/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CDAppURL : NSURL
+ (void)setHost:(NSString*)host;
+ (void)setPort:(NSInteger)port;
- (id)initWithPath:(NSString*)path parameters:(NSDictionary*)parameters;
@end
