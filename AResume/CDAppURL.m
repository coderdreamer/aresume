//
//  CDURL.m
//  AResume
//
//  Created by William Remaerd on 12/29/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDAppURL.h"
#import "CDString.h"
#define kHead [NSString stringWithFormat:@"%@:%d", gHost, gPort]
@interface CDAppURL (Private)
@end

@implementation CDAppURL

static NSString *gHost = @"173.208.240.146";
static NSInteger gPort = 8070;

+ (void)setHost:(NSString *)host{
    gHost = host;
}

+ (void)setPort:(NSInteger)port{
    gPort = port;
}

- (id)initWithPath:(NSString *)path parameters:(NSDictionary *)parameters{
    NSMutableString *absolutePath = [[NSMutableString alloc] initWithFormat:@"http://%@",
                                     [kHead stringByAppendingPathComponent:path]];
    char separater = '?';
    for (NSString* key in parameters.allKeys) {
        NSString* value = [parameters objectForKey:key];
        if (key.isVisuallyEmpty || value.isVisuallyEmpty) continue;
        [absolutePath appendFormat:@"%c%@=%@",separater, key, value];
        if (separater == '?') separater = '&';
    }
    self = [super initWithString:absolutePath];
    [absolutePath release];
    
    if (self) {
    }
    return self;
}
@end
