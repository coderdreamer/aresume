//
//  CDButton.h
//  AResume
//
//  Created by William Remaerd on 12/24/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGOImageButton.h"

@interface CDButton : EGOImageButton

@end
