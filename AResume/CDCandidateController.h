//
//  CDCandidateController.h
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDSideviewController.h"
#define kCDCandidateControllerXib @"CDCandidateController"
@class CDImageView, Candidate;
@interface CDCandidateController : CDSideviewSubcontroller <UITableViewDataSource, UITableViewDelegate>
@property(nonatomic, retain)IBOutlet CDImageView *photo;
@property(nonatomic, retain)IBOutlet UILabel *name;
@property(nonatomic, retain)IBOutlet UITableView *infoTable;

@property(nonatomic, retain)Candidate *candidate;

@end
