//
//  CDCandidateController.m
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDCandidateController.h"
#import "Header.h"
#import "Candidate.h"
#import "CoreDataModels.h"
#import "CDAppURL.h"
#import "CDImageView.h"

@interface CDCandidateController ()
@end

@implementation CDCandidateController

- (id)initWithSideviewController:(CDSideviewController *)sideviewController embededNavigation:(BOOL)embeded{
    self = [super initWithNibName:kCDCandidateControllerXib bundle:nil];
    self = [super initWithSideviewController:sideviewController embededNavigation:embeded];
    if (self) {
        self.candidate = [AppDelegate sharedCnadidate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"关于我";
    _name.text = _candidate.name;
    _photo.placeholderImage = _photo.image;
    [_candidate cancelSynchronization];
    [self synchronizeModel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Model
- (void)synchronizeModel{
    CDCloudSynCompletion completion = ^(CDCloudModel *model, CDCloudSynState state){
        if (state == CDCloudSynStateSuccessful) {
            self.candidate = (Candidate *)model;
            Candidate *candidate = (Candidate *)model;
            _name.text = candidate.name;
            [_infoTable reloadData];
            
            NSString *path = candidate.photo;
            CDAppURL *url = [[CDAppURL alloc] initWithPath:path parameters:nil];
            [_photo setImageURL:url];
            [url release];
        }
    };
    [_candidate synchronizeWithCompletion:completion];
}

#pragma mark - UITableViewDataSource
#define kCellIdentifier @"infoTable"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:kCellIdentifier];
        [cell autorelease];
    }
    switch (indexPath.section) {
        case 0:{
            switch (indexPath.row) {
                case 0:{
                    cell.textLabel.text = kStringGender;
                    cell.detailTextLabel.text = _candidate.genderString;
                }break;
                case 1:{
                    cell.textLabel.text = kStringAge;
                    cell.detailTextLabel.text = _candidate.ageString;
                }break;
                default:
                    break;
            }
        }break;
        case 1:{
            Education* education = [_candidate.education.allObjects objectAtIndex:indexPath.row];
            cell.textLabel.text = education.degreeString;
            NSString *university = [[NSString alloc] initWithFormat:@"%@-%@", education.university, education.major];
            cell.detailTextLabel.text = university;
            [university release];
        }break;
        case 2:{
            //NSUInteger number = [_candidate.contact.allObjects count];
            Contact* contact = [_candidate.contact.allObjects objectAtIndex:indexPath.row];
            cell.textLabel.text = contact.typeString;
            cell.detailTextLabel.text = contact.value;
        }break;
        default:
            break;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger number = 0;
    switch (section) {
        case 0:
            number =  2;
            break;
        case 1:
            number = _candidate.education.count;
            break;
        case 2:
            number = _candidate.contact.count;
            break;
        default:
            break;
    }
    
    return number;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString * title = nil;
    switch (section) {
        case 0:{
            title = kStringIndividualInfo;
        }break;
        case 1:{
            if(_candidate.education.count != 0) title = kStringEducation;
        }break;
        case 2:{
            if(_candidate.contact.count != 0) title = kStringContact;
        }break;
        default:
            break;
    }
    return title;
}

#pragma mark - UITableViewDelegate

@end
