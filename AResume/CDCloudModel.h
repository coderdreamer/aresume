//
//  CDCloudModel.h
//  AResume
//
//  Created by William Remaerd on 12/20/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class CDCloudSyn;
@interface CDCloudModel : NSManagedObject
{CDCloudSyn* _syn;}

@property (nonatomic) int16_t cid;

@end
