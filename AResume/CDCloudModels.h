//
//  CDCloudModel.h
//  AResume
//
//  Created by William Remaerd on 12/19/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "CDCloudSyn.h"
#import "CDCloudModel.h"

#define kKeyCID @"cid"
@class CDCloudModel, ASIHTTPRequest, ASINetworkQueue;
@interface CDCloudModel (CDCloudModels)
/*
 {CDCloudSyn* _syn;}
 */
+ (id)getFROMByCID:(int16_t)cid completion:(CDCloudSynCompletion)completion;
+ (id)getFROMByCID:(int16_t)cid moduleName:(NSString*)name completion:(CDCloudSynCompletion)completion;
+ (NSManagedObjectContext*)context;
- (void)removeFromContext;
- (BOOL)addCompletion:(CDCloudSynCompletion)completion;
- (CDCloudModel*)relationship:(SEL)getter witIndex:(NSInteger)index key:(NSString*)key;

- (void)synchronizeWithCompletion:(CDCloudSynCompletion)completion;
- (void)synchronizeWithTranslator:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion;
@end

@interface CDCloudModel (Private)
- (void)setupWithDictionarry:(NSDictionary*)dictionary;
- (void)synchronizeProperty:(Class)propertyClass withArray:(NSArray*)newArray;
- (void)synchronizeProperty:(NSString*)propertyName withArray:(NSArray*)newArray getMethod:(SEL)getter addMethod:(SEL)addMethod deleteMethod:(SEL)delMethod;
- (void)fireSynWithURL:(NSURL*)url translator:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion;
- (void)cancelSynchronization;
@end