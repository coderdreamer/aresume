//
//  CDCloudModel.m
//  AResume
//
//  Created by William Remaerd on 12/19/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDCloudModel.h"
#import "Header.h"
#import "CDCloudModels.h"
#import "CDCategories.h"



@implementation CDCloudModel (CDCloudModels)

+ (id)getFROMByCID:(int16_t)cid moduleName:(NSString*)name completion:(CDCloudSynCompletion)completion{
    NSManagedObjectContext* context = [CDCloudModel context];
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"cid == %d", cid];
    NSFetchRequest* fetch = [[NSFetchRequest alloc] initWithEntityName:name];
    [fetch setPredicate:predicate];
    NSArray* results = [context executeFetchRequest:fetch error:nil];
    [fetch release];
    
    CDCloudModel* model = nil;
    if (results.count == 1) {
        model = [results objectAtIndex:0];
    }else if (results.count == 0){
        model = [NSEntityDescription insertNewObjectForEntityForName:name inManagedObjectContext:context];
        model.cid = cid;
    }
    CDCloudSynCompletion synCompletion = ^(CDCloudModel *model, CDCloudSynState state){
        switch (state) {
            case CDCloudSynStateNotFound:{
                [model removeFromContext];
            }break;
            default:
                break;
        }
        if (completion) completion(model, state);
    };
    [model synchronizeWithCompletion:synCompletion];
    
    return model;
}
+ (id)getFROMByCID:(int16_t)cid completion:(CDCloudSynCompletion)completion{return nil;}

+ (NSManagedObjectContext*)context{
    NSManagedObjectContext* context = [(AppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
    return context;
}

- (void)setupWithDictionarry:(NSDictionary*)dictionary{
    int16_t cid = [[dictionary objectForKey:kKeyCID] intValue];
    self.cid = cid;
}

- (void)synchronizeProperty:(Class)propertyClass withArray:(NSArray*)newArray{
    NSString *propertyName = NSStringFromClass(propertyClass);
    
    NSString *firstChar = [[propertyName substringToIndex:1] lowercaseString];
    NSString *getterName = [firstChar stringByAppendingString:[propertyName substringFromIndex:1]];
    
    NSString *addName = [[NSString alloc] initWithFormat:@"%@%@%@", @"add", propertyName, @"Object:"];
    NSString *delName = [[NSString alloc] initWithFormat:@"%@%@%@", @"remove", propertyName, @"Object:"];
    
    [self synchronizeProperty:propertyName withArray:newArray getMethod:NSSelectorFromString(getterName) addMethod:NSSelectorFromString(addName) deleteMethod:NSSelectorFromString(delName)];
    
    [delName release];
    [addName release];
}

- (void)synchronizeProperty:(NSString*)propertyName withArray:(NSArray*)newArray getMethod:(SEL)getter addMethod:(SEL)addMethod deleteMethod:(SEL)delMethod{
    if (!(propertyName && newArray && getter && addMethod)) return;
    
    CDSynSetEquality completion = ^(id rObject, id oObject){
        int16_t rID = [(CDCloudModel*)rObject cid];
        int oID = [[(NSDictionary*)oObject objectForKey:kKeyCID] intValue];
        BOOL isEqual = (rID == oID);
        return isEqual;
    };
    NSMutableSet *r = [[NSMutableSet alloc] initWithSet:[self performSelector:getter]];
    NSMutableSet *o = [[NSMutableSet alloc] initWithArray:newArray];
    NSSet *i = [r intersectSet:o withEquality:completion];
    
    NSManagedObjectContext* context = [CDCloudModel context];
    for (CDCloudModel *model in r) {
        [self performSelector:delMethod withObject:model];
        [context deleteObject:model];
    }
    for (NSDictionary *dic in o) {
        CDCloudModel *model = [NSEntityDescription insertNewObjectForEntityForName:propertyName inManagedObjectContext:context];
        [model setupWithDictionarry:dic];
        [self performSelector:addMethod withObject:model];
    }
    for (NSDictionary *iDic in i) {
        CDCloudModel *model = [iDic objectForKey:kSynSetKeyReciever];
        NSDictionary *dic = [iDic objectForKey:kSynSetKeyOther];
        [model setupWithDictionarry:dic];
    }
    
    [r release];
    [o release];
}

- (BOOL)addCompletion:(CDCloudSynCompletion)completion{
    if (_syn != nil && _syn.completion == nil) {
        _syn.completion = completion;
        return YES;
    }
    return NO;
}

- (CDCloudModel*)relationship:(SEL)getter witIndex:(NSInteger)index key:(NSString*)key{
    NSSet *relationship = [self performSelector:getter];
    if (relationship.count <= index) return nil;
    
    NSSortDescriptor *des = [[NSSortDescriptor alloc] initWithKey:key ascending:YES];
    NSArray *descriptors = [[NSArray alloc] initWithObjects:des, nil];
    NSArray *array = [relationship sortedArrayUsingDescriptors:descriptors];
    CDCloudModel *object = [array objectAtIndex:index];

    [des release];
    [descriptors release];
    
    return object;
}

#pragma mark - Synchronize
- (void)synchronizeWithCompletion:(CDCloudSynCompletion)completion{}
- (void)synchronizeWithTranslator:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion{}

- (void)fireSynWithURL:(NSURL*)url translator:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion{
    _syn = [[CDCloudSyn alloc] initWithURL:url delegate:self];
    _syn.canBeReleased = @selector(releaseSyn:);
    [_syn synchronizeWithTranslater:translator completion:completion];
}

- (void)cancelSynchronization{
    if (_syn != nil) {
        [_syn cancelSynchronization];
        MemberRelease(_syn);
    }
}

- (void)releaseSyn:(CDCloudModel*)cloudModel{
    MemberRelease(_syn);
}

#pragma mark - Managed Object
- (void)removeFromContext{
    NSManagedObjectContext* context = [(AppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
    [context deleteObject:self];
}

- (void)dealloc{
    DLogMethod;
    
    MemberRelease(_syn);
    [super dealloc];
}
@end

