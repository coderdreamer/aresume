//
//  CDCloudSyn.h
//  AResume
//
//  Created by William Remaerd on 12/20/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    CDCloudSynStateSuccessful,
    CDCloudSynStateFailed,
    CDCloudSynStateUnexpired,
    CDCloudSynStateNotFound
}CDCloudSynState;
@class CDCloudModel, ASIHTTPRequest, ASINetworkQueue;
typedef void(^CDCloudSynCompletion)(CDCloudModel*, CDCloudSynState);
typedef CDCloudModel*(^CDCloudTranslator)(NSData*);

@interface CDCloudSyn : NSObject{
    ASIHTTPRequest* _request;
    CDCloudSynCompletion _completion;
    CDCloudTranslator _translator;
    id _delegate;
    SEL _canBeReleased;
}
@property(nonatomic, copy)CDCloudTranslator translator;
@property(nonatomic, copy)CDCloudSynCompletion completion;
@property(nonatomic, assign)SEL canBeReleased;
- (id)initWithURL:(NSURL*)url delegate:(id)delegate;
- (void)synchronizeWithTranslater:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion;
- (void)cancelSynchronization;
- (ASINetworkQueue*)queue;
+ (ASINetworkQueue*)sharedQueue;

@end

typedef BOOL(^CDSynSetEquality)(id, id);
#define kSynSetKeyReciever @"Rec"
#define kSynSetKeyOther @"Oth"
@interface NSMutableSet (CDSynSet)
/**
 After insersetion, the reciever & otherSet will be minused the insersetion, and return the insersetion consists of NSDictionary.
 @param otherSet The set with which to perform the intersection.
 @param equality The Block indicates two objects are the same or not.
 @return A NSSet consists of NSDictionary.The dictionary consists of two key:object, kSynSetKeyReciever and kSynSetKeyOther.
 */
- (NSSet*)intersectSet:(NSMutableSet*)otherSet withEquality:(CDSynSetEquality)equality;
@end