//
//  CDCloudSyn.m
//  AResume
//
//  Created by William Remaerd on 12/20/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDCloudSyn.h"
#import "Header.h"
#import "ASIHTTPRequest.h"
#import "ASINetworkQueue.h"

@interface CDCloudSyn()
- (void)requestFinished:(ASIHTTPRequest*)request;
- (void)requestFailed:(ASIHTTPRequest*)request;
@end

@implementation CDCloudSyn
@synthesize translator = _translator, completion = _completion;

#pragma mark - Life Cycle
- (id)initWithURL:(NSURL*)url delegate:(id)delegate{
    self = [super init];
    if (self && url) {
        _request = [[ASIHTTPRequest alloc] initWithURL:url];
        _request.delegate = self;
        _request.didFinishSelector = @selector(requestFinished:);
        _request.didFailSelector = @selector(requestFailed:);
        _delegate = delegate;
    }
    return self;
}

- (void)dealloc{
    DLogMethod;
    
    MemberRelease(_request);
    MemberRelease(_translator);
    MemberRelease(_completion);
    [super dealloc];
}

#pragma mark - ASIHttpRequest delegate
- (void)requestFinished:(ASIHTTPRequest*)request{
    NSData* responseData = request.responseData;
    
    CDCloudModel* model = _translator(responseData);
    _completion(model, CDCloudSynStateSuccessful);
    if (_delegate && [_delegate respondsToSelector:_canBeReleased]) {
        [_delegate performSelector:_canBeReleased];
    }
}

- (void)requestFailed:(ASIHTTPRequest*)request{
    _completion(nil, CDCloudSynStateFailed);
    DLog(@"%@", request.error.userInfo);
    if (_delegate && [_delegate respondsToSelector:_canBeReleased]) {
        [_delegate performSelector:_canBeReleased];
    }
}

#pragma mark - ASIHttpRequest Synchronize
- (void)synchronizeWithTranslater:(CDCloudTranslator)translator completion:(CDCloudSynCompletion)completion{
    self.translator = translator;
    self.completion = completion;
    [self.queue addOperation:_request];
}

- (void)cancelSynchronization{
    [_request clearDelegatesAndCancel];
    MemberRelease(_translator);
    MemberRelease(_completion);
}

#pragma mark - Queue
- (ASINetworkQueue*)queue{
    ASINetworkQueue* queue = [self.class sharedQueue];
    return queue;
}

+ (NSOperationQueue*)sharedQueue{
    static ASINetworkQueue* sharedQueue = nil;
    @synchronized(sharedQueue){
        if (sharedQueue == nil){
            sharedQueue = [[ASINetworkQueue alloc] init];
            [sharedQueue setShouldCancelAllRequestsOnFailure:NO];
            [sharedQueue go];
        }
    }
    return sharedQueue;
}

@end

@implementation NSMutableSet (CDSynSet)
- (NSSet*)intersectSet:(NSMutableSet*)otherSet withEquality:(CDSynSetEquality)equality{
    if (otherSet == nil || equality == nil) return nil;
    NSMutableSet *rIntersectSet = [[NSMutableSet alloc] init];
    NSMutableSet *oIntersectSet = [[NSMutableSet alloc] init];
    NSMutableSet *intersetSet = [[NSMutableSet alloc] init];
    for (id rObject in self) {
        for (id oObject in otherSet) {
            if (equality(rObject, oObject)) {
                [rIntersectSet addObject:rObject];
                [oIntersectSet addObject:oObject];
                NSDictionary* dic = [[NSDictionary alloc] initWithObjectsAndKeys:rObject,kSynSetKeyReciever, oObject, kSynSetKeyOther, nil];
                [intersetSet addObject:dic];
                [dic release];
            }
        }
    }
    
    [self minusSet:rIntersectSet];
    [otherSet minusSet:oIntersectSet];
    
    NSSet *returnSet = [NSSet setWithSet:intersetSet];
    
    [rIntersectSet release];
    [oIntersectSet release];
    [intersetSet release];
    return returnSet;
}

@end