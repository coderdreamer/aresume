//
//  CDExperienceController.h
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSideviewController.h"
#define kCDExperienceControllerXib @"CDExperienceController"
@class UIFolderTableView, Candidate;
@interface CDExperienceController : CDSideviewSubcontroller <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, retain) NSArray *experiences;
@property (nonatomic, retain) IBOutlet UIFolderTableView *tableView;
@end
