//
//  CDExperienceController.m
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDExperienceController.h"
#import "Header.h"
#import "Candidate.h"
#import "CoreDataModels.h"
#import "UIFolderTableView.h"
#import "CDFolderTableViewCell.h"
#import "CDFolderShowView.h"

@interface CDExperienceController ()
@end

@implementation CDExperienceController

- (id)initWithSideviewController:(CDSideviewController *)sideviewController embededNavigation:(BOOL)embeded{
    self = [super initWithNibName:kCDExperienceControllerXib bundle:nil];
    self = [super initWithSideviewController:sideviewController embededNavigation:embeded];
    if (self) {
        Candidate* candidate = [AppDelegate sharedCnadidate];
        NSSet* experiences = candidate.experience;
        if (experiences == nil || experiences.count == 0) {
            [self synchronizeModel];
        }else{
            self.experiences = experiences.allObjects;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的经验";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Model
- (void)synchronizeModel{
    Candidate* candidate = [AppDelegate sharedCnadidate];
    CDCloudSynCompletion completion = ^(CDCloudModel *model, CDCloudSynState state){
        if (state == CDCloudSynStateSuccessful) {
            Candidate* candidate = (Candidate*)model;
            self.experiences = candidate.experience.allObjects;
            [_tableView reloadData];
        }
    };
    [candidate synchronizeExperiencesWithCompletion:completion];
}

#pragma mark - UITableViewDataSource
#define kCellIdentifier @"infoTable"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CDFolderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[CDFolderTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:kCellIdentifier];
        [cell autorelease];
    }
    Experience* experience = [_experiences objectAtIndex:indexPath.row];
    cell.title.text = experience.title;
    [cell setIconPath:experience.firstImage];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger number = _experiences.count;
    return number;
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Experience* experience = [_experiences objectAtIndex:indexPath.row];
    CDFolderShowView *view = [[[CDFolderShowView alloc] initWithWidth:self.view.bounds.size.width description:experience.exDescription] autorelease];
    [view setImagePath:experience.secondImage];
    UIFolderTableView *folderTableView = (UIFolderTableView *)tableView;
    [folderTableView openFolderAtIndexPath:indexPath WithContentView:view
                                 openBlock:nil
                                closeBlock:nil
                           completionBlock:nil];
}

@end
