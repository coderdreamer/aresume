//
//  CDFolderShowView.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CDImageView;
@interface CDFolderShowView : UIView
@property(strong, nonatomic)IBOutlet CDImageView *image;
@property(strong, nonatomic)IBOutlet UILabel *description;
- (id)initWithWidth:(CGFloat)width description:(NSString*)description;
- (void)setImagePath:(NSString*)path;
@end
