//
//  CDFolderShowView.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDFolderShowView.h"
#import "CDImageView.h"
#import "Header.h"
#import "CDAppURL.h"

@implementation CDFolderShowView

- (id)initWithWidth:(CGFloat)width description:(NSString*)description{
    self = [super initWithFrame:CGRectMake(0.0f, 0.0f, width, width)];
    if (self) {
        UIView *xibView = [self loadSubviewsFromXibNamed:NSStringFromClass([CDFolderShowView class])];

        CGFloat bottomMargin = xibView.bounds.size.height - CGRectGetMaxY(_description.frame);
        CGFloat currentWidth = _description.frame.size.width;
        CGFloat minHeight = _description.frame.size.height;
        CGFloat lineHeight = _description.font.lineHeight;
        CGSize size = [description sizeWithFont:_description.font
                                    constrainedToSize:CGSizeMake(currentWidth, CGFLOAT_MAX)
                                        lineBreakMode:_description.lineBreakMode];
        _description.numberOfLines = size.height / lineHeight;
        CGFloat height = size.height < minHeight ? minHeight : size.height;
        _description.frame = CGRectMake(_description.frame.origin.x, _description.frame.origin.y, currentWidth, height);
        _description.text = description;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.frame.size.width, CGRectGetMaxY(_description.frame) + bottomMargin);
    }
    return self;
}

- (void)setImagePath:(NSString*)path{
    if (path == nil || [_image.imageURL.path isEqualToString:path]) return;
    
    CDAppURL *url = [[CDAppURL alloc] initWithPath:path parameters:nil];
    _image.imageURL = url;
    [url release];
}

@end
