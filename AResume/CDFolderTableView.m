#import "CDFolderTableView.h"

@interface UIFolderTableView ()
- (void)performClose:(id)sender;
@end

@implementation CDFolderTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)openFolderAtIndexPath:(NSIndexPath *)indexPath WithContentView:(UIView *)subClassContentView openBlock:(FolderOpenBlock)openBlock closeBlock:(FolderCloseBlock)closeBlock completionBlock:(FolderCompletionBlock)completionBlock{
    self.scrollEnabled = NO;
    [super openFolderAtIndexPath:indexPath WithContentView:subClassContentView openBlock:openBlock closeBlock:closeBlock completionBlock:completionBlock];
}

- (void)performClose:(id)sender{
    [super performClose:sender];
    self.scrollEnabled = YES;
}

@end
