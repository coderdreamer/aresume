//
//  CDFolderTableViewCell.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#define kCDFolderTableViewCellXib @"CDFolderTableViewCell"
#define kCellHeight 50.0f
@class CDImageView;
@interface CDFolderTableViewCell : UITableViewCell
@property(strong, nonatomic)IBOutlet CDImageView *icon;
@property(strong, nonatomic)IBOutlet UILabel *title;
- (void)setIconPath:(NSString*)path;
@end
