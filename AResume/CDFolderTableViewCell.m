//
//  CDFolderTableViewCell.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDFolderTableViewCell.h"
#import "CDCategories.h"
#import "CDImageView.h"
#import "CDAppURL.h"

@implementation CDFolderTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self loadSubviewsFromXibNamed:kCDFolderTableViewCellXib];
        [_icon initWithPlaceholderImage:[UIImage pngImageWithName:@"DefaultPhoto"]];
    }
    return self;
}

- (void)setIconPath:(NSString*)path{
    if (path == nil || [_icon.imageURL.path isEqualToString:path]) return;
    
    CDAppURL *url = [[CDAppURL alloc] initWithPath:path parameters:nil];
    _icon.imageURL = url;
    [url release];
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
	[super willMoveToSuperview:newSuperview];
	
	if(!newSuperview) {
		[_icon cancelImageLoad];
	}
}


@end
