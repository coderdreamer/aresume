//
//  CDHonourController.h
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSideviewController.h"
#define kCDHonourControllerXib @"CDHonourController"
@class CDFolderTableView;
@interface CDHonourController : CDSideviewSubcontroller <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic)NSArray* honour;
@property (strong, nonatomic)IBOutlet CDFolderTableView *tableView;
@end
