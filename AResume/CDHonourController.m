//
//  CDHonourController.m
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDHonourController.h"
#import "Header.h"
#import "Candidate.h"
#import "CoreDataModels.h"

@interface CDHonourController ()
@end

@implementation CDHonourController
#pragma mark - View Controller
- (id)initWithSideviewController:(CDSideviewController *)sideviewController embededNavigation:(BOOL)embeded{
    self = [super initWithNibName:kCDHonourControllerXib bundle:nil];
    self = [super initWithSideviewController:sideviewController embededNavigation:embeded];
    if (self) {
        Candidate* candidate = [AppDelegate sharedCnadidate];
        NSArray* honour = candidate.sortedHonour;
        if (honour == nil || honour.count == 0) {
            [self synchronizeModel];
        }else{
            self.honour = honour;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"在校期间";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Model
- (void)synchronizeModel{
    Candidate* candidate = [AppDelegate sharedCnadidate];
    CDCloudSynCompletion completion = ^(CDCloudModel *model, CDCloudSynState state){
        if (state == CDCloudSynStateSuccessful) {
            Candidate* candidate = (Candidate*)model;
            self.honour = candidate.sortedHonour;
            [_tableView reloadData];
        }
    };
    [candidate synchronizeHonourWithCompletion:completion];
}

#pragma mark - UITableViewDataSource
#define kCellIdentifier @"infoTable"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellIdentifier];
        [cell autorelease];
    }
    Honour* honour = [_honour objectAtIndex:indexPath.row];
    cell.textLabel.text = honour.title;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger number = _honour.count;
    return number;
}
#pragma mark - UITableViewDelegate

@end
