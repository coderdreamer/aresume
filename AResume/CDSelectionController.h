//
//  CDSelectionController.h
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CDSideviewController.h"
#define kInfoCardXib @"CDInfoCard"
#define kInfoCardHeight 44.0f
@class CDButton;
@class Candidate;
@interface CDSelectionController : CDSideviewSubcontroller <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, retain)Candidate *candidate;

@property(nonatomic, retain)UITableView *tableView;
@property(nonatomic, retain)UIView *infoCard;
@property(nonatomic, retain)IBOutlet CDButton *photo;
@property(nonatomic, retain)IBOutlet UIButton *name;

- (id)initWithCandidate:(Candidate*)candidate;

@end
