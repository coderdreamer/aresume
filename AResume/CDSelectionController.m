//
//  CDSelectionController.m
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSelectionController.h"
#import "Header.h"
#import "CDCloudModels.h"
#import "Candidate.h"
#import "CDAResumeViewController.h"
#import "CDExperienceController.h"
#import "CDSkillController.h"
#import "CDHonourController.h"
#import "CDButton.h"
#import "CDAppURL.h"

@interface CDSelectionController ()
- (IBAction)clickPhotoOrName:(id)sender;
@end

@implementation CDSelectionController

- (void)initialize{
    }

- (id)initWithCandidate:(Candidate *)candidate{
    self = [super init];
    if (self) {
        self.candidate = candidate;
        [self initialize];
    }
    return self;
}

- (id)init{
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)dealloc{
    MemberRelease(_infoCard);
    MemberRelease(_tableView);
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _infoCard = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.view.bounds.size.width, kInfoCardHeight)];
    [self.view addSubview:_infoCard], [_infoCard release];
    [_infoCard loadSubviewsFromXibNamed:kInfoCardXib owner:self];
    _photo.placeholderImage = _photo.imageView.image;
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.0f, CGRectGetMaxY(_infoCard.frame), self.view.bounds.size.width, self.view.bounds.size.height - CGRectGetHeight(_infoCard.frame))];
    [self.view addSubview:_tableView], [_tableView release];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    
    self.candidate = [AppDelegate sharedCnadidate];
    [_candidate synchronizeWithCompletion:^(CDCloudModel *model, CDCloudSynState state){
        if (state == CDCloudSynStateSuccessful) {
            //_name.titleLabel.text = _candidate.name;
            Candidate *candidate = (Candidate *)model;
            [_name setTitle:candidate.name forState:UIControlStateNormal];
            
            NSString *path = candidate.photo;
            CDAppURL *url = [[CDAppURL alloc] initWithPath:path parameters:nil];
            [_photo setImageURL:url];
            [url release];

        }
    }];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    NSInteger index = NSIntegerMax;
    NSString *identifier = _sideviewController.currentIdentifier;
    if ([identifier isEqualToString:kIdentifierExperience]) {
        index = 0;
    }else if ([identifier isEqualToString:kIdentifierSkill]) {
        index = 1;
    }else if ([identifier isEqualToString:kIdentifierHonour]) {
        index = 2;
    }
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    [_tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
}

- (void)viewDidDisappear:(BOOL)animated{
    [_tableView deselectRowAtIndexPath:_tableView.indexPathForSelectedRow animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma maek - Info Card
- (IBAction)clickPhotoOrName:(id)sender{
    [_sideviewController switchToRegisteredControllerByIndentifier:kIdentifierCandidate];
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    switch (indexPath.row) {
        case 0:{
            cell.textLabel.text = @"经验";
        }break;
        case 1:{
            cell.textLabel.text = @"技能";
        }break;
        case 2:{
            cell.textLabel.text = @"在校情况";
        }break;
        default:
            break;
    }
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString* identifier = nil;
    switch (indexPath.row) {
        case 0:{
            identifier = kIdentifierExperience;
        }break;
        case 1:{
            identifier = kIdentifierSkill;
        }break;
        case 2:{
            identifier = kIdentifierHonour;
        }break;
        default:
            break;
    }
    [_sideviewController switchToRegisteredControllerByIndentifier:identifier];
}

@end
