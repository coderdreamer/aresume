//
//  CDSideviewController.h
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "DDMenuController.h"
#define kDefaultOffsetLeft 80.0f
#define kKeyClassName @"Class"
#define kKeyEmbededNavigation @"Embeded"
#define kKeyInstance @"Instance"
@class CDSideviewSubcontroller;
@interface CDSideviewController : DDMenuController {
    NSMutableDictionary *_controllers;
}
@property(nonatomic, copy)NSString* currentIdentifier;

- (id)initWithRootViewController:(UIViewController *)rootViewController identifier:(NSString*)identifier;
- (NSMutableDictionary*)registerControllerWithIdentifier:(NSString*)identifier conClass:(Class)conClass embededInNavigation:(BOOL)embeded;
- (void)clearSleepingController;
- (CDSideviewSubcontroller*)switchToRegisteredControllerByIndentifier:(NSString *)identifier;
- (CDSideviewSubcontroller*)registeredControllerByIdentifier:(NSString*)identifier;

@end

@interface CDSideviewSubcontroller : UIViewController{
    CDSideviewController* _sideviewController;
    BOOL _embededNavigation;
}
@property(nonatomic, readonly, assign)CDSideviewController* sideviewController;
@property(nonatomic, readonly, assign)BOOL embededNavigation;
- (id)initWithSideviewController:(CDSideviewController*)sideviewController embededNavigation:(BOOL)embeded;
- (void)synchronizeModel;
@end
