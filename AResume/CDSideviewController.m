//
//  CDSideviewController.m
//  AResume
//
//  Created by William Remaerd on 12/23/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSideviewController.m"
#import "CDSelectionController.h"
#import "Header.h"
#import "CDCandidateController.h"

@interface CDSideviewController ()
- (void)initialize;
- (CDSideviewSubcontroller*)createRegisteredControllerByIdentifier:(NSString*)indentifier;
- (CDSideviewSubcontroller*)destroyRegisteredControllerByIdentifier:(NSString*)indentifier;
@end

@implementation CDSideviewController

#pragma mark - UIViewConrtroller
- (id)initWithRootViewController:(UIViewController *)rootViewController identifier:(NSString*)identifier{
    BOOL embeded = rootViewController.navigationController != nil;
    UIViewController* controller = embeded ? rootViewController.navigationController : rootViewController;
    self = [super initWithRootViewController:controller];
    if (self) {
        [self initialize];
        NSMutableDictionary* info = [self registerControllerWithIdentifier:identifier conClass:rootViewController.class embededInNavigation:embeded];
        [info setObject:rootViewController forKey:kKeyInstance];
        self.currentIdentifier = identifier;
    }
    return self;
}

- (void)initialize{
    _controllers = [[NSMutableDictionary alloc] init];
    [self setupLeftController];
}

- (void)dealloc{
    MemberRelease(_controllers);
    MemberRelease(_currentIdentifier);
    [super dealloc];
}

- (void)didReceiveMemoryWarning{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Inherit
- (void)synchronizeModel{
    
}

- (void)clearSleepingController{
    
}

- (CDSideviewSubcontroller*)createRegisteredControllerByIdentifier:(NSString*)indentifier{
    return nil;
}

- (CDSideviewSubcontroller*)destroyRegisteredControllerByIdentifier:(NSString*)indentifier{
    return nil;
}

#pragma mark - Controller Manage
- (NSMutableDictionary*)registerControllerWithIdentifier:(NSString*)identifier conClass:(Class)conClass embededInNavigation:(BOOL)embeded{
    NSString *className = NSStringFromClass(conClass);
    NSNumber *embededValue = [[NSNumber alloc] initWithBool:embeded];
    CDSideviewSubcontroller* instance = (CDSideviewSubcontroller*)[NSNull null];
    
    NSMutableDictionary* info = [[NSMutableDictionary alloc] initWithObjectsAndKeys:className, kKeyClassName, embededValue, kKeyEmbededNavigation, instance, kKeyInstance, nil];
    
    [_controllers setObject:info forKey:identifier];
    
    [info release];
    [embededValue release];
    
    return info;
}

- (CDSideviewSubcontroller*)registeredControllerByIdentifier:(NSString*)identifier{
    NSAssert(identifier != nil, @"Identifier should not be nil.");
    
    NSMutableDictionary *info = [_controllers objectForKey:identifier];
    CDSideviewSubcontroller *controller = [info objectForKey:kKeyInstance];
    if (controller == (CDSideviewSubcontroller*)[NSNull null]) {
        NSString *className = [info objectForKey:kKeyClassName];
        controller = [NSClassFromString(className) alloc];
        BOOL embeded = [[info objectForKey:kKeyEmbededNavigation] boolValue];
        [controller initWithSideviewController:self embededNavigation:embeded];
        [info setObject:controller forKey:kKeyInstance];
    }
    
    return controller;
}

#pragma mark - Switch
- (CDSideviewSubcontroller*)switchToRegisteredControllerByIndentifier:(NSString *)identifier{
    CDSideviewSubcontroller *subcontroller = [self registeredControllerByIdentifier:identifier];
    UIViewController *controller = subcontroller.embededNavigation ? subcontroller.navigationController : subcontroller;
    [self setRootController:controller animated:YES];
    self.currentIdentifier = identifier;
    return subcontroller;
}

#pragma mark - Left
- (void)setupLeftController{
    CDSelectionController* controller = [[CDSelectionController alloc] initWithSideviewController:self embededNavigation:NO];
    self.leftViewController = controller;
}

- (void)showLeftController{
    [self showLeftController:YES];
}

@end


@implementation CDSideviewSubcontroller : UIViewController
@synthesize sideviewController = _sideviewController, embededNavigation = _embededNavigation;
- (id)initWithSideviewController:(CDSideviewController*)sideviewController embededNavigation:(BOOL)embeded{
    self = [super init];
    if (self) {
        _sideviewController = sideviewController;
        _embededNavigation = embeded;
        if (embeded) {
            UINavigationController *navigation = [[UINavigationController alloc] initWithRootViewController:self];
            [self.navigationController retain];
            [navigation release];
        }
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithTitle:@"更新" style:UIBarButtonItemStylePlain target:self action:@selector(synchronizeModel)];
    self.navigationItem.rightBarButtonItem = right;
    [right release];
}

- (void)dealloc{
    if (_embededNavigation) {
        [self.navigationController release];
    }
    [super dealloc];
}

- (void)synchronizeModel{
    
}

- (void)clearSleepingController{
    
}

- (CDSideviewSubcontroller*)switchToRegisteredControllerByIndentifier:(NSString *)identifier{
    return nil;
}

- (CDSideviewSubcontroller*)registeredControllerByIdentifier:(NSString*)identifier{
    return nil;
}

@end