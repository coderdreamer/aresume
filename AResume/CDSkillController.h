//
//  CDSkillController.h
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSideviewController.h"
#define kCDSkillControllerXib @"CDSkillController"
@class CDFolderTableView;
@interface CDSkillController : CDSideviewSubcontroller <UITableViewDataSource, UITableViewDelegate>
@property (strong, nonatomic)NSArray* skills;
@property (strong, nonatomic)IBOutlet CDFolderTableView *tableView;
@end
