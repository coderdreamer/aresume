//
//  CDSkillController.m
//  AResume
//
//  Created by William Remaerd on 12/25/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "CDSkillController.h"
#import "Header.h"
#import "Candidate.h"
#import "CoreDataModels.h"
#import "UIFolderTableView.h"
#import "CDFolderTableViewCell.h"
#import "CDFolderShowView.h"

@interface CDSkillController ()
@end

@implementation CDSkillController
#pragma mark - View Controller
- (id)initWithSideviewController:(CDSideviewController *)sideviewController embededNavigation:(BOOL)embeded{
    self = [super initWithNibName:kCDSkillControllerXib bundle:nil];
    self = [super initWithSideviewController:sideviewController embededNavigation:embeded];
    if (self) {
        Candidate* candidate = [AppDelegate sharedCnadidate];
        NSSet* skills = candidate.skill;
        if (skills == nil || skills.count == 0) {
            [self synchronizeModel];
        }else{
            self.skills = skills.allObjects;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationItem.title = @"我的技能";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Model
- (void)synchronizeModel{
    Candidate* candidate = [AppDelegate sharedCnadidate];
    CDCloudSynCompletion completion = ^(CDCloudModel *model, CDCloudSynState state){
        if (state == CDCloudSynStateSuccessful) {
            Candidate* candidate = (Candidate*)model;
            self.skills = candidate.skill.allObjects;
            [_tableView reloadData];
        }
    };
    [candidate synchronizeSkillsWithCompletion:completion];
}

#pragma mark - UITableViewDataSource
#define kCellIdentifier @"infoTable"
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CDFolderTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (cell == nil) {
        cell = [[CDFolderTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:kCellIdentifier];
        [cell autorelease];
    }
    Skill* skill = [_skills objectAtIndex:indexPath.row];
    cell.title.text = skill.title;
    //cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setIconPath:skill.firstImage];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSInteger number = _skills.count;
    return number;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Skill* experience = [_skills objectAtIndex:indexPath.row];
    CDFolderShowView *view = [[[CDFolderShowView alloc] initWithWidth:self.view.bounds.size.width description:experience.skillDescription] autorelease];
    UIFolderTableView *folderTableView = (UIFolderTableView *)tableView;
    [folderTableView openFolderAtIndexPath:indexPath WithContentView:view
                                 openBlock:nil
                                closeBlock:nil
                           completionBlock:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kCellHeight;
}
@end
