//
//  Candidate.h
//  AResume
//
//  Created by William Remaerd on 12/30/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"

@class Contact, Education, Experience, Honour, Skill;

@interface Candidate : CDCloudModel

@property (nonatomic) NSTimeInterval dateOfBirth;
@property (nonatomic) int16_t gender;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * photo;
@property (nonatomic, retain) NSSet *contact;
@property (nonatomic, retain) NSSet *education;
@property (nonatomic, retain) NSSet *experience;
@property (nonatomic, retain) NSSet *honour;
@property (nonatomic, retain) NSSet *skill;
@end

@interface Candidate (CoreDataGeneratedAccessors)

- (void)addContactObject:(Contact *)value;
- (void)removeContactObject:(Contact *)value;
- (void)addContact:(NSSet *)values;
- (void)removeContact:(NSSet *)values;

- (void)addEducationObject:(Education *)value;
- (void)removeEducationObject:(Education *)value;
- (void)addEducation:(NSSet *)values;
- (void)removeEducation:(NSSet *)values;

- (void)addExperienceObject:(Experience *)value;
- (void)removeExperienceObject:(Experience *)value;
- (void)addExperience:(NSSet *)values;
- (void)removeExperience:(NSSet *)values;

- (void)addHonourObject:(Honour *)value;
- (void)removeHonourObject:(Honour *)value;
- (void)addHonour:(NSSet *)values;
- (void)removeHonour:(NSSet *)values;

- (void)addSkillObject:(Skill *)value;
- (void)removeSkillObject:(Skill *)value;
- (void)addSkill:(NSSet *)values;
- (void)removeSkill:(NSSet *)values;

@end
