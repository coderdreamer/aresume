//
//  Candidate.m
//  AResume
//
//  Created by William Remaerd on 12/30/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Candidate.h"
#import "Contact.h"
#import "Education.h"
#import "Experience.h"
#import "Honour.h"
#import "Skill.h"


@implementation Candidate

@dynamic dateOfBirth;
@dynamic gender;
@dynamic name;
@dynamic photo;
@dynamic contact;
@dynamic education;
@dynamic experience;
@dynamic honour;
@dynamic skill;

@end
