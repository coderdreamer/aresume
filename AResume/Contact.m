//
//  Contact.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Contact.h"
#import "Candidate.h"


@implementation Contact

@dynamic type;
@dynamic value;
@dynamic candidate;

@end
