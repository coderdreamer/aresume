#import <CoreData/CoreData.h>
#import "CDCloudSyn.h"
#import "CDCloudModels.h"

#import "Candidate.h"
#define kStringGender @"性别"
#define kStringAge @"年龄"
#define kStringIndividualInfo @"个人信息"
#define kKeyName @"Nam"
#define kKeyGender @"Gen"
#define kKeyDateOfBirth @"DOB"
#define kKeyPhoto @"Pho"
#define kKeyContact @"Con"
#define kKeyEducation @"Edu"
typedef enum {
    GenderFemael = 0,
    GenderMale = 1,
}Gender;
@interface Candidate (CDCandidate)
- (void)synchronizeExperiencesWithCompletion:(CDCloudSynCompletion)completion;
- (void)synchronizeSkillsWithCompletion:(CDCloudSynCompletion)completion;
- (void)synchronizeHonourWithCompletion:(CDCloudSynCompletion)completion;
- (NSString*)genderString;
- (NSString*)ageString;
- (NSArray*)sortedHonour;
@end

#import "Contact.h"
#define kStringContact @"联系方式"
#define kKeyType @"Type"
#define kKeyValue @"Value"
typedef enum {
    ContactTypeMobile = 0,
    ContactTypeEmail = 1,
}ContactType;
@interface Contact (CDContact)
- (NSString*)typeString;
@end

#import "Education.h"
#define kStringEducation @"教育背景"
#define kKeyDegree @"Degree"
#define kKeyMajor @"Major"
#define kKeyUniversity @"Uni"
typedef enum {
    EducationDegreeBachelor = 0,
}EducationDegreee;
@interface Education (CDEducation)
- (NSString*)degreeString;
@end

#import "Experience.h"
#define kKeyDescription @"Description"
#define kKeyLink @"Link"
#define kKeyTitle @"Title"
#define kKeyImage @"Image"
@interface Experience (CDExperience)
- (NSString*)firstImage;
- (NSString*)secondImage;
@end

#import "Skill.h"
@interface Skill (CDSkill)
- (NSString*)firstImage;
@end

#import "Honour.h"
#define kKeyDate @"Date"
@interface Honour (CDHonour)
@end

#import "Image.h"
#define kKeyPath @"Path"
#define kKeyIndex @"Index"
@interface Image (CDImage)
@end
