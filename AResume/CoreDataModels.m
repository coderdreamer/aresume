//
//  CoreDataModels.m
//  AResume
//
//  Created by William Remaerd on 12/24/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//
#import "CoreDataModels.h"
#import "Header.h"
#import "CDCategories.h"
#import "CDAppURL.h"

@implementation Candidate (CDCandidate)
#pragma mark - Cloud Model
+ (id)getFROMByCID:(int16_t)cid completion:(CDCloudSynCompletion)completion{
    Candidate* candidate = [CDCloudModel getFROMByCID:cid moduleName:NSStringFromClass([Candidate class]) completion:completion];
    return candidate;
}

- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    self.name = [dictionary objectForKey:kKeyName];
    self.gender = [[dictionary objectForKey:kKeyGender] intValue];
    
    NSString *dateString = [dictionary objectForKey:kKeyDateOfBirth];
    NSDate *date = [NSDate dateFromString:dateString withFormat:@"yyyy-MM-dd"];
    self.dateOfBirth = date.timeIntervalSince1970;
    
    self.photo = [dictionary objectForKey:kKeyPhoto];
    
    NSArray *education = [dictionary objectForKey:kKeyEducation];
    [self synchronizeProperty:[Education class] withArray:education];
    NSArray *contact = [dictionary objectForKey:kKeyContact];
    [self synchronizeProperty:[Contact class] withArray:contact];
}

#pragma mark - Synchronize Property
- (void)synchronizeWithCompletion:(CDCloudSynCompletion)completion{
    if (_syn == nil) {
        CDCloudTranslator translator = ^(NSData* data){
            //Translate Json into model
            if (data != nil) {
                NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [self setupWithDictionarry:jsonDic];
            }
            _syn.translator = nil;
            return self;
        };
        
        CDAppURL *url = [[CDAppURL alloc] initWithPath:@"/Candidate" parameters:@{@"id":[NSString stringWithFormat:@"%d",self.cid]}];
        [self fireSynWithURL:url translator:translator completion:completion];
        [url release];
    }
}

- (void)synchronizeExperiencesWithCompletion:(CDCloudSynCompletion)completion{
    if (_syn == nil) {
        CDCloudTranslator translator = ^(NSData* data){
            //Translate Json into model
            if (data != nil) {
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [self synchronizeProperty:[Experience class] withArray:jsonArray];
            }
            
            _syn.translator = nil;
            return self;
        };
        CDAppURL *url = [[CDAppURL alloc] initWithPath:@"/RelExperience" parameters:@{@"id":[NSString stringWithFormat:@"%d",self.cid]}];
        [self fireSynWithURL:url translator:translator completion:completion];
        [url release];
    }
}

- (void)synchronizeSkillsWithCompletion:(CDCloudSynCompletion)completion{
    if (_syn == nil) {
        CDCloudTranslator translator = ^(NSData* data){
            //Translate Json into model
            if (data != nil) {
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [self synchronizeProperty:[Skill class] withArray:jsonArray];
            }
            
            _syn.translator = nil;
            return self;
        };
        CDAppURL *url = [[CDAppURL alloc] initWithPath:@"/RelSkill" parameters:@{@"id":[NSString stringWithFormat:@"%d",self.cid]}];
        [self fireSynWithURL:url translator:translator completion:completion];
        [url release];
    }
}

- (void)synchronizeHonourWithCompletion:(CDCloudSynCompletion)completion{
    if (_syn == nil) {
        CDCloudTranslator translator = ^(NSData* data){
            //Translate Json into model
            if (data != nil) {
                NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                [self synchronizeProperty:[Honour class] withArray:jsonArray];
            }
            
            _syn.translator = nil;
            return self;
        };
        CDAppURL *url = [[CDAppURL alloc] initWithPath:@"/RelHonour" parameters:@{@"id":[NSString stringWithFormat:@"%d",self.cid]}];
        [self fireSynWithURL:url translator:translator completion:completion];
        [url release];
    }
}

#pragma mark - Getter
- (NSString*)genderString{
    switch (self.gender) {
        case GenderFemael:{
            return @"靓妹";
        }break;
        case GenderMale:{
            return @"帅哥";
        }break;
        default:
            break;
    }
    return @"有待探索";
}

- (NSString*)ageString{
    NSTimeInterval second = [[NSDate date] timeIntervalSince1970] - self.dateOfBirth;
    NSUInteger year = second / (60 * 60 * 24 * 365);
    NSString *string = [NSString stringWithFormat:@"%d", year + 1];
    return string;
}

- (NSArray*)sortedHonour{
    NSSet *relationship = self.honour;
    NSSortDescriptor *des = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *descriptors = [[NSArray alloc] initWithObjects:des, nil];
    NSArray *array = [relationship sortedArrayUsingDescriptors:descriptors];
    [des release];
    [descriptors release];
    
    return array;
}
@end

@implementation Contact (CDContact)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    NSNumber* type = [dictionary objectForKey:kKeyType];
    self.type  = type.intValue;
    self.value = [dictionary objectForKey:kKeyValue];
}

- (NSString*)typeString{
    switch (self.type) {
        case 0:{
            return @"爪机";
        }break;
        case 1:{
            return @"邮箱";
        }break;
        default:
            break;
    }
    return @"神器";
}
@end

@implementation Education (CDEducation)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    self.degree = [[dictionary objectForKey:kKeyDegree] intValue];
    self.university = [dictionary objectForKey:kKeyUniversity];
    self.major = [dictionary objectForKey:kKeyMajor];
}

- (NSString*)degreeString{
    switch (self.degree) {
        case EducationDegreeBachelor:{
            return @"本科";
        }break;
        default:
            break;
    }
    return @"家里蹲";
}
@end

@implementation Experience (CDExperience)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    self.title = [dictionary objectForKey:kKeyTitle];
    self.exDescription = [dictionary objectForKey:kKeyDescription];
    
    NSArray* newImageArray = [dictionary objectForKey:kKeyImage];
    [self synchronizeProperty:[Image class] withArray:newImageArray];
}

- (NSString*)firstImage{
    Image *image = (Image *)[self relationship:@selector(image) witIndex:0 key:@"index"];
    return image.path;
}

- (NSString*)secondImage{
    Image *image = (Image *)[self relationship:@selector(image) witIndex:1 key:@"index"];
    return image.path;
}
@end

@implementation Skill (CDSkill)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    self.title = [dictionary objectForKey:kKeyTitle];
    self.skillDescription = [dictionary objectForKey:kKeyDescription];
    NSArray *image = [dictionary objectForKey:kKeyImage];
    [self synchronizeProperty:[Image class] withArray:image];
}

- (NSString*)firstImage{
    Image *image = (Image *)[self relationship:@selector(image) witIndex:0 key:@"index"];
    return image.path;
}
@end

@implementation Honour (CDHonour)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    self.title = [dictionary objectForKey:kKeyTitle];
    
    NSString *dateString = [dictionary objectForKey:kKeyDate];
    NSDate *date = [NSDate dateFromString:dateString withFormat:@"yyyy-MM-dd"];
    self.date = date.timeIntervalSince1970;
}

@end

@implementation Image (CDImage)
- (void)setupWithDictionarry:(NSDictionary *)dictionary{
    [super setupWithDictionarry:dictionary];
    self.path = [dictionary objectForKey:kKeyPath];
    self.index = [[dictionary objectForKey:kKeyIndex] intValue];
}
@end
