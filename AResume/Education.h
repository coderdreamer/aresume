//
//  Education.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"

@class Candidate;

@interface Education : CDCloudModel

@property (nonatomic) int16_t degree;
@property (nonatomic, retain) NSString * major;
@property (nonatomic, retain) NSString * university;
@property (nonatomic, retain) Candidate *candidate;

@end
