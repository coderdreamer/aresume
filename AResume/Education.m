//
//  Education.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Education.h"
#import "Candidate.h"


@implementation Education

@dynamic degree;
@dynamic major;
@dynamic university;
@dynamic candidate;

@end
