//
//  Experience.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"

@class Candidate, Image;

@interface Experience : CDCloudModel

@property (nonatomic, retain) NSString * exDescription;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Candidate *candidate;
@property (nonatomic, retain) NSSet *image;
@end

@interface Experience (CoreDataGeneratedAccessors)

- (void)addImageObject:(Image *)value;
- (void)removeImageObject:(Image *)value;
- (void)addImage:(NSSet *)values;
- (void)removeImage:(NSSet *)values;

@end
