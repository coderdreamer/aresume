//
//  Experience.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Experience.h"
#import "Candidate.h"
#import "Image.h"


@implementation Experience

@dynamic exDescription;
@dynamic link;
@dynamic title;
@dynamic candidate;
@dynamic image;

@end
