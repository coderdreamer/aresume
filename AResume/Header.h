//
//  Header.h
//  HoldLanguages
//
//  Created by William Remaerd on 11/10/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "AppDelegate.h"
#import "CDCategories.h"

#define kTestImagePath0 @"http://img.91xoyo.com/images/s_imgs/58117_77.jpg"
#define kTestImagePath1 @"http://img1.ezhun.com/p/07/509936/91856_109x129.jpg"
#define kTestImagePath2 @"http://img13.c2.ku6.cn/desPicPath/2009/5/1/4/395233/5.jpg"

#define kMissLocalizedString @"MissLocalizedString"
#define kDebugColor [UIColor colorWithRed:0.5f green:0.5f blue:0.5f alpha:0.5f]
#define kViewAutoresizingNoMarginSurround UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight

#define MemberRelease(object) [object release], object = nil;
#define ViewMemberRelease(object) [object removeFromSuperview], [object release], object = nil;


#define DEBUG_MODE
#ifdef DEBUG_MODE

#define DLog(...);      NSLog(__VA_ARGS__);
#define DLogPoint(p)    NSLog(@"%f,%f", p.x, p.y);
#define DLogSize(p)     NSLog(@"%f,%f", p.width, p.height);
#define DLogRect(p)     NSLog(@"%f,%f %f,%f", p.origin.x, p.origin.y, p.size.width, p.size.height);
#define  __FILENAME__  [[NSString stringWithCString:__FILE__ encoding:NSStringEncodingConversionAllowLossy] lastPathComponent]
#define DLogCurrentMethod NSLog(@"%@:%d@%@/%@", __FILENAME__, __LINE__, NSStringFromClass([self class]), NSStringFromSelector(_cmd))
#define DLogMethod NSLog(@"%@-%@", NSStringFromClass([self class]), NSStringFromSelector(_cmd))

#else

#define DLog(...);      // NSLog(__VA_ARGS__);
#define DLogPoint(p)    // NSLog(@"%f,%f", p.x, p.y);
#define DLogSize(p)     // NSLog(@"%f,%f", p.width, p.height);
#define DLogRect(p)     // NSLog(@"%f,%f %f,%f", p.origin.x, p.origin.y, p.size.width, p.size.height);

#endif
