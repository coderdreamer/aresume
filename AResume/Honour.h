//
//  Honour.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"

@class Candidate;

@interface Honour : CDCloudModel

@property (nonatomic) NSTimeInterval date;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Candidate *candidate;

@end
