//
//  Honour.m
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Honour.h"
#import "Candidate.h"


@implementation Honour

@dynamic date;
@dynamic title;
@dynamic candidate;

@end
