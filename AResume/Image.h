//
//  Image.h
//  AResume
//
//  Created by William Remaerd on 12/26/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"


@interface Image : CDCloudModel

@property (nonatomic) int16_t index;
@property (nonatomic, retain) NSString * path;

@end
