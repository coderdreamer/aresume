//
//  Skill.h
//  AResume
//
//  Created by William Remaerd on 12/31/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDCloudModel.h"

@class Candidate, Image;

@interface Skill : CDCloudModel

@property (nonatomic, retain) NSString * skillDescription;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Candidate *candidate;
@property (nonatomic, retain) NSSet *image;
@end

@interface Skill (CoreDataGeneratedAccessors)

- (void)addImageObject:(Image *)value;
- (void)removeImageObject:(Image *)value;
- (void)addImage:(NSSet *)values;
- (void)removeImage:(NSSet *)values;

@end
