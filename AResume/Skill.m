//
//  Skill.m
//  AResume
//
//  Created by William Remaerd on 12/31/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "Skill.h"
#import "Candidate.h"
#import "Image.h"


@implementation Skill

@dynamic skillDescription;
@dynamic title;
@dynamic candidate;
@dynamic image;

@end
