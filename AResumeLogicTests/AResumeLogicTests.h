//
//  AResumeLogicTests.h
//  AResumeLogicTests
//
//  Created by William Remaerd on 12/20/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import <SenTestingKit/SenTestingKit.h>
@class CDCloudModel, Candidate;
@interface AResumeLogicTests : SenTestCase{
    Candidate* _candidate;
    NSManagedObjectModel* _model;
    NSPersistentStoreCoordinator* _coordinator;
    NSManagedObjectContext* _context;
}

@end
