//
//  AResumeLogicTests.m
//  AResumeLogicTests
//
//  Created by William Remaerd on 12/20/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "AResumeLogicTests.h"
#import "CDCloudModel.h"
#import "CDCloudModels.h"
#import "Candidate.h"
#import "CDCloudSyn.h"
#import "CDAppURL.h"

@implementation AResumeLogicTests

- (void)setUp{
    [super setUp];
    
    _model = [[NSManagedObjectModel mergedModelFromBundles:[NSBundle allBundles]] retain];
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
    _context = [[NSManagedObjectContext alloc] init];
    [_context setPersistentStoreCoordinator:_coordinator];
    [_context setRetainsRegisteredObjects:YES];
    
    _candidate = [[NSEntityDescription insertNewObjectForEntityForName:@"Candidate" inManagedObjectContext:_context] retain];
}

- (void)tearDown{
    [_candidate release];
    [_context release];
    [_coordinator release];
    [_model release];
    [super tearDown];
}

- (void)testIntersectSet{
    NSNumber *number1 = [NSNumber numberWithInteger:1];
    NSNumber *number2 = [NSNumber numberWithInteger:2];
    NSNumber *number3 = [NSNumber numberWithInteger:3];
    NSString *string2 = [NSString stringWithFormat:@"%d", 2];
    NSString *string3 = [NSString stringWithFormat:@"%d", 3];
    NSString *string4 = [NSString stringWithFormat:@"%d", 4];
    
    CDSynSetEquality equality = ^(id rObject, id oObject){
        NSInteger r = [(NSNumber *)rObject integerValue];
        NSInteger o = [(NSString *)oObject integerValue];
        BOOL isEqual = (r == o);
        return isEqual;
    };
    
    NSMutableSet *rSet0 = [NSMutableSet setWithObjects:number1, number2, number3, nil];
    NSMutableSet *oSet0 = [NSMutableSet setWithObjects:string2, string3, string4, nil];
    NSSet *rAnswer0 = [NSMutableSet setWithObjects:number1, nil];
    NSSet *oAnswer0 = [NSMutableSet setWithObjects:string4, nil];
    NSSet *iAnswer0 = [NSMutableSet setWithObjects:
                       @{kSynSetKeyReciever:number2, kSynSetKeyOther:string2},
                       @{kSynSetKeyReciever:number3, kSynSetKeyOther:string3}, nil];
    NSSet *interset = [rSet0 intersectSet:oSet0 withEquality:equality];
    STAssertTrueNoThrow([rSet0 isEqualToSet:rAnswer0], @"Receiever set wrong.");
    STAssertTrueNoThrow([oSet0 isEqualToSet:oAnswer0], @"Other set wrong.");
    STAssertTrueNoThrow([interset isEqualToSet:iAnswer0], @"Interset set wrong.");
}

- (void)testCDAppURL{
    NSDictionary *parameters = @{@"id":@"1"};
    CDAppURL *url = [[CDAppURL alloc] initWithPath:@"candidate" parameters:parameters];
    STAssertTrueNoThrow([url.absoluteString isEqualToString:@"127.0.0.1:7777/candidate?id=1"], @"testCDAppURL Wrong.");
    [url release];
    /*
    url = [[CDAppURL alloc] initWithPath:@"candidate" parameters:nil];
    STAssertTrueNoThrow([url isEqualToString:@"127.0.0.1:7777/candidate"], @"testCDAppURL Wrong.");
    [url release];
    */
    parameters = @{@"id":@"1", @"a":@"a", @"b":@"b"};
    url = [[CDAppURL alloc] initWithPath:@"candidate" parameters:parameters];
    STAssertTrueNoThrow([url.absoluteString isEqualToString:@"127.0.0.1:7777/candidate?id=1&a=a&b=b"], @"testCDAppURL Wrong.");
    [url release];    
    
    parameters = @{@"id":@"1", @"":@"a", @"b":@""};
    url = [[CDAppURL alloc] initWithPath:@"candidate" parameters:parameters];
    STAssertTrueNoThrow([url.absoluteString isEqualToString:@"127.0.0.1:7777/candidate?id=1"], @"testCDAppURL Wrong.");
    [url release];
}

@end
