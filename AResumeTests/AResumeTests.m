//
//  AResumeTests.m
//  AResumeTests
//
//  Created by William Remaerd on 12/19/12.
//  Copyright (c) 2012 Coder Dreamer. All rights reserved.
//

#import "AResumeTests.h"
#import "CDCloudModel.h"
#import "CDCloudModels.h"
#import "Candidate.h"
#import "Header.h"
#import "Contact.h"

@implementation AResumeTests

- (void)setUp
{
    [super setUp];
    NSManagedObjectContext* context = [(AppDelegate*)[UIApplication sharedApplication].delegate managedObjectContext];
    _candidate = [NSEntityDescription insertNewObjectForEntityForName:@"Candidate" inManagedObjectContext:context];
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    [(AppDelegate*)[UIApplication sharedApplication].delegate saveContext];
    [super tearDown];
}

- (void)testModelSynchronize{
    CDCloudSynCompletion completion = ^(CDCloudModel* model, CDCloudSynState state){
        Candidate* candidate = (Candidate*)model;
        STAssertTrueNoThrow([candidate isMemberOfClass:[Candidate class]], @"Wrong Object Class Type:%@", candidate.class);
    };
    [_candidate synchronizeWithCompletion:completion];
}

- (void)testModelGetFROMByCid{
    CDCloudSynCompletion completion = ^(CDCloudModel* model, CDCloudSynState state){
        Candidate* candidate = (Candidate*)model;
        STAssertTrueNoThrow([candidate isMemberOfClass:[Candidate class]], @"Wrong Object Class Type:%@", candidate.class);
    };
    [Candidate getFROMByCID:7 completion:completion];
}

@end
